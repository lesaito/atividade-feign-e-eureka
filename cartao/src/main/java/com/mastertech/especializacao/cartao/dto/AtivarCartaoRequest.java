package com.mastertech.especializacao.cartao.dto;

public class AtivarCartaoRequest {
    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
