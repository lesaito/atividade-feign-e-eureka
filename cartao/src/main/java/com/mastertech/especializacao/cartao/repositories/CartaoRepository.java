package com.mastertech.especializacao.cartao.repositories;

import com.mastertech.especializacao.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    public Optional<Cartao> findCartaoByNumero(String numero);
}