package com.mastertech.especializacao.cartao.models;

import javax.persistence.*;

@Entity
public class Cartao {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="cartao_id")
    private int id;

    @Column(nullable = false, unique = true)
    private String numero;

    private int clienteId;

    @Column(nullable = false)
    private Boolean ativo;

    public Cartao(String numero, int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public Cartao () {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCliente() {
        return clienteId;
    }

    public void setCliente(int clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
