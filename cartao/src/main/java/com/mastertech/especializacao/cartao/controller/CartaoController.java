package com.mastertech.especializacao.cartao.controller;

import com.mastertech.especializacao.cartao.dto.AtivarCartaoRequest;
import com.mastertech.especializacao.cartao.dto.CartaoRequest;
import com.mastertech.especializacao.cartao.models.Cartao;
import com.mastertech.especializacao.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<Cartao> registrar(@Valid @RequestBody CartaoRequest cartaoRequest) {
        Cartao cartao = cartaoService.cadastrar(cartaoRequest.getNumero(), cartaoRequest.getClienteId());
        return new ResponseEntity<Cartao>(cartao, HttpStatus.CREATED);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<Cartao> alterar(@PathVariable(value = "numero") String numero,
                                          @Valid @RequestBody AtivarCartaoRequest ativarCartaoRequest) {
        return new ResponseEntity<Cartao>(cartaoService.alterar(numero, ativarCartaoRequest.isAtivo()), HttpStatus.OK);
    }

    @GetMapping("/{numero}")
    public ResponseEntity<Cartao> buscar(@PathVariable(value = "numero") String numero) {
        Cartao cartao = cartaoService.buscar(numero);
        if (cartao.equals(null)) {
            return new ResponseEntity<Cartao>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Cartao>(cartao, HttpStatus.OK);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Cartao> buscar(@PathVariable(value = "id") int id) {
        Cartao cartao = cartaoService.buscarPorId(id);
        if (cartao.equals(null)) {
            return new ResponseEntity<Cartao>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Cartao>(cartao, HttpStatus.OK);
        }
    }

}
