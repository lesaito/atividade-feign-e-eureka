package com.mastertech.especializacao.cartao.clients;

import com.mastertech.especializacao.cartao.clients.dtos.ClienteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    public ClienteDTO buscaClientePorId(@PathVariable(value = "id") int id);
}
