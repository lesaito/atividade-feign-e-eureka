package com.mastertech.especializacao.cartao.services;

import com.mastertech.especializacao.cartao.repositories.CartaoRepository;
import com.mastertech.especializacao.cartao.clients.ClienteClient;
import com.mastertech.especializacao.cartao.clients.dtos.ClienteDTO;
import com.mastertech.especializacao.cartao.exceptions.ClienteNotFoundException;
import com.mastertech.especializacao.cartao.models.Cartao;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteClient clienteClient;

    public Cartao cadastrar (String numero, int idCliente) {
        ClienteDTO cliente = null;
        try {
            cliente = clienteClient.buscaClientePorId(idCliente);
        //} catch (FeignException.FeignClientException.NotFound e) {
        } catch (HystrixRuntimeException e) {
            if (e.getCause() instanceof ClienteNotFoundException) {
                throw new ClienteNotFoundException();
            }
        }

        Cartao cartao = new Cartao(numero, cliente.getId());
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao alterar (String numero, boolean ativar) {
        Optional<Cartao> cartao = cartaoRepository.findCartaoByNumero(numero);
        cartao.get().setAtivo(ativar);
        return cartaoRepository.save(cartao.get());
    }

    public Cartao buscar (String numero) {
        Optional<Cartao> cartao = cartaoRepository.findCartaoByNumero(numero);
        return cartao.get();
    }

    public Cartao buscarPorId(int id) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        return cartao.get();
    }
}