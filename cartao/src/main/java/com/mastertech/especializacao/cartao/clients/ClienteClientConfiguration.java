package com.mastertech.especializacao.cartao.clients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClienteClientConfiguration {

    @Bean
    public ClienteDecoder decode() {
        return new ClienteDecoder();
    }
}
