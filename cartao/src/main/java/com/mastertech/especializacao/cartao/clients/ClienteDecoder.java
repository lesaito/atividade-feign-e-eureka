package com.mastertech.especializacao.cartao.clients;

import com.mastertech.especializacao.cartao.exceptions.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new ClienteNotFoundException();
        }
        return null;
    }
}
