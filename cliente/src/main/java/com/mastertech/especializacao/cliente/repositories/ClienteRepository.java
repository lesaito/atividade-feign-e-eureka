package com.mastertech.especializacao.cliente.repositories;

import com.mastertech.especializacao.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}