package com.mastertech.especializacao.cliente.mappers;

import com.mastertech.especializacao.cliente.dtos.ClienteResponse;
import com.mastertech.especializacao.cliente.models.Cliente;

public class ClienteMapper {

    public static ClienteResponse toClienteResponse(Cliente cliente){
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());

        return clienteResponse;
    }
}
