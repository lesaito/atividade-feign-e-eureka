package com.mastertech.especializacao.cliente.controllers;

import com.mastertech.especializacao.cliente.dtos.ClienteRequest;
import com.mastertech.especializacao.cliente.dtos.ClienteResponse;
import com.mastertech.especializacao.cliente.mappers.ClienteMapper;
import com.mastertech.especializacao.cliente.models.Cliente;
import com.mastertech.especializacao.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<ClienteResponse> registrar(@Valid @RequestBody ClienteRequest clienteRequest) {
      Cliente cliente = new Cliente(clienteRequest.getName());
      return new ResponseEntity<ClienteResponse>(ClienteMapper.toClienteResponse(clienteService.cadastrar(cliente)), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClienteResponse GetById(@PathVariable(value = "id") int id) {
        return ClienteMapper.toClienteResponse(clienteService.buscar(id));
    }
}
