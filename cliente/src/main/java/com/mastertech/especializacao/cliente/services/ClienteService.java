package com.mastertech.especializacao.cliente.services;

import com.mastertech.especializacao.cliente.exceptions.ClienteNotFoundException;
import com.mastertech.especializacao.cliente.models.Cliente;
import com.mastertech.especializacao.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscar(int id) {
        Optional<Cliente> cliente  = clienteRepository.findById(id);
        if (cliente.isPresent()) {
            return cliente.get();
        } else {
            throw new ClienteNotFoundException();
        }
    }
}
