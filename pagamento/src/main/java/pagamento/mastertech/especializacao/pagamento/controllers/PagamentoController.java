package pagamento.mastertech.especializacao.pagamento.controllers;

import pagamento.mastertech.especializacao.pagamento.dtos.CadastroPagamentoRequest;
import pagamento.mastertech.especializacao.pagamento.models.Pagamento;
import pagamento.mastertech.especializacao.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> cadastrar (@Valid @RequestBody CadastroPagamentoRequest cadastroPagamentoRequest) {
        return new ResponseEntity<Pagamento>(pagamentoService.cadastrar(cadastroPagamentoRequest), HttpStatus.OK);
    }

    @GetMapping("/{id_cartao}")
    public Iterable<Pagamento> buscar (@PathVariable(value = "id_cartao") int idCartao) {
        return pagamentoService.buscar(idCartao);
    }
}
