package pagamento.mastertech.especializacao.pagamento.services;

import feign.FeignException;
import pagamento.mastertech.especializacao.pagamento.clients.CartaoClient;
import pagamento.mastertech.especializacao.pagamento.clients.dtos.CartaoDTO;
import pagamento.mastertech.especializacao.pagamento.dtos.CadastroPagamentoRequest;
import pagamento.mastertech.especializacao.pagamento.exceptions.CartaoNotFoundException;
import pagamento.mastertech.especializacao.pagamento.models.Pagamento;
import pagamento.mastertech.especializacao.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Pagamento cadastrar (CadastroPagamentoRequest pagamentoRequest) {
        CartaoDTO cartao = null;
        try {
            cartao = cartaoClient.buscaCartaoPorId(pagamentoRequest.getCartao_id());
        } catch (FeignException.FeignClientException e ) {
            throw new CartaoNotFoundException();
        }

        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(cartao.getId());
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());

        return pagamentoRepository.save(pagamento);
    }

    public Iterable<Pagamento> buscar (int cartaoId) {
        CartaoDTO cartao = null;
        try {
            cartao = cartaoClient.buscaCartaoPorId(cartaoId);
        } catch (FeignException.FeignClientException e ) {
            throw new CartaoNotFoundException();
        }
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }
}
