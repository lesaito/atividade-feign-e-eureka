package pagamento.mastertech.especializacao.pagamento.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pagamento.mastertech.especializacao.pagamento.clients.dtos.CartaoDTO;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    public CartaoDTO buscaCartaoPorId(@PathVariable(value = "id") int id);
}
