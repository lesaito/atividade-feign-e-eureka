package pagamento.mastertech.especializacao.pagamento.repositories;

import pagamento.mastertech.especializacao.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    public Iterable<Pagamento> findAllByCartaoId(int cartaoId);
}
