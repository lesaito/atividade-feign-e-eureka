package com.especializacao.itauspec1.cartoes.pagamento.services;

import com.especializacao.itauspec1.cartoes.cartao.services.CartaoService;
import com.especializacao.itauspec1.cartoes.pagamento.dtos.CadastroPagamentoRequest;
import com.especializacao.itauspec1.cartoes.cartao.models.Cartao;
import com.especializacao.itauspec1.cartoes.pagamento.models.Pagamento;
import com.especializacao.itauspec1.cartoes.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public Pagamento cadastrar (CadastroPagamentoRequest pagamentoRequest) {
        Cartao cartao = cartaoService.buscarPorId(pagamentoRequest.getCartao_id());
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(cartao);
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());

        return pagamentoRepository.save(pagamento);
    }

    public Iterable<Pagamento> buscar (int idCartao) {
        Cartao cartao = cartaoService.buscarPorId(idCartao);
        return pagamentoRepository.findAllByCartao(cartao);
    }
}
