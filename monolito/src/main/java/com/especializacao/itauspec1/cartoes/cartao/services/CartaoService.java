package com.especializacao.itauspec1.cartoes.cartao.services;

import com.especializacao.itauspec1.cartoes.cartao.models.Cartao;
import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;
import com.especializacao.itauspec1.cartoes.cartao.repositories.CartaoRepository;
import com.especializacao.itauspec1.cartoes.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;

    public Cartao cadastrar (String numero, int idCliente) {
        Cliente cliente = clienteService.buscar(idCliente);
        Cartao cartao = new Cartao(numero, cliente);
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao alterar (String numero, boolean ativar) {
        Optional<Cartao> cartao = cartaoRepository.findCartaoByNumero(numero);
        cartao.get().setAtivo(ativar);
        return cartaoRepository.save(cartao.get());
    }

    public Cartao buscar (String numero) {
        Optional<Cartao> cartao = cartaoRepository.findCartaoByNumero(numero);
        return cartao.get();
    }

    public Cartao buscarPorId(int id) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        return cartao.get();
    }
}