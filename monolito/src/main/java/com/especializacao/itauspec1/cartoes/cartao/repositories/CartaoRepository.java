package com.especializacao.itauspec1.cartoes.cartao.repositories;

import com.especializacao.itauspec1.cartoes.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    public Optional<Cartao> findCartaoByNumero(String numero);
}