package com.especializacao.itauspec1.cartoes.cliente.dtos;

public class ClienteRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
