package com.especializacao.itauspec1.cartoes.cliente.repositories;

import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}