package com.especializacao.itauspec1.cartoes.cliente.mappers;

import com.especializacao.itauspec1.cartoes.cliente.dtos.ClienteResponse;
import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;

public class ClienteMapper {

    public static ClienteResponse toClienteResponse(Cliente cliente){
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());

        return clienteResponse;
    }
}
