package com.especializacao.itauspec1.cartoes.cliente.services;

import com.especializacao.itauspec1.cartoes.cliente.exceptions.ClienteNotFoundException;
import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;
import com.especializacao.itauspec1.cartoes.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscar(int id) {
        Optional<Cliente> cliente  = clienteRepository.findById(id);
        if (cliente.isPresent()) {
            return cliente.get();
        } else {
            throw new ClienteNotFoundException();
        }
    }
}
