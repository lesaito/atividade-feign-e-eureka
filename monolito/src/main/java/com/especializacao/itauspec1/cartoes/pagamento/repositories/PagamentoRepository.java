package com.especializacao.itauspec1.cartoes.pagamento.repositories;

import com.especializacao.itauspec1.cartoes.cartao.models.Cartao;
import com.especializacao.itauspec1.cartoes.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    public Iterable<Pagamento> findAllByCartao(Cartao cartao);
}
